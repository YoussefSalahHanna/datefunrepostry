import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import * as globals from './globals';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeServiceService {
  responseData: any;
  constructor(
    private http: Http,
  ) { }
  listLanguages(): Observable<any> {
    return this.http
      .get(globals.url.apiUrl + '/dmlanguage/list')
      .map(res => {
        this.responseData = res.json();
        return this.responseData;
      });
  }
  viewLanguage(id): Observable<any> {
    return this.http
      .get(globals.url.apiUrl + '/dmfunction/list?langId=' + id)
      .map(res => {
        this.responseData = res.json();
        return this.responseData;
      });
  }
  examplesApi(langId, functionId): Observable<any> {
    return this.http
      .get(globals.url.apiUrl + '/dmexample/example?langId=' + langId + '&functionId=' + functionId)
      .map(res => {
        this.responseData = res.json();
        return this.responseData;
      });
  }
}