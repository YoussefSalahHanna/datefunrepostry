import { Component, OnInit } from '@angular/core';
import { HomeServiceService } from "../services/home-service.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  languageList: Array<any> = [];
  oneLanguage: Array<any> = [];
  constructor(
    private service: HomeServiceService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.listLanguages();
  }
  listLanguages() {
    this.service.listLanguages().subscribe(
      response => {
        if (response && response.length) {
          this.languageList = response;
          this.route.params.subscribe(params => {
            if (params['langname']) {
              this.loadLanguage(params['langname']);
            } else {
              this.loadLanguage(response[0].Id);
            }
          });
        }
      }
    )
  }
  loadLanguage(langId) {
    this.oneLanguage = [];
    this.service.viewLanguage(langId).subscribe(
      response => {
        if (response && response.length) {
          this.oneLanguage = response;
          console.log(this.oneLanguage)
        }
      }
    )
  }
}
